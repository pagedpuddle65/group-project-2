<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ page import="edu.a.project2.bo.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.io.StringWriter" %>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../../styles/form.css" type="text/css">
<title>Asset Tracking</title>


</head>
<body>
<!-- setup baseUrl variable that may be used later.! -->
<%
	String url = request.getRequestURL().toString();
	String baseURL = url.substring(0, url.length() - request.getRequestURI().length()) + request.getContextPath() + "/";
%>

<!--if "user" attribute is null in session then go back to index.jsp for user to login -->
<% 
	if((Object)session.getAttribute("user") == null)
	response.sendRedirect(baseURL + "index.jsp"); 
%>
		
<!-- Grab all the parameters and save the asset -->
<%
	String offices = request.getParameter("offices");
	String tag = request.getParameter("tag");
	String manufacturer = request.getParameter("manufacturer");
	String part = request.getParameter("part");
	String description = request.getParameter("description");
	String notes = request.getParameter("notes");
	
	/* If we have an office set in the request it means we are trying to save a new asset. */
	if(offices != null){
		//Create the new asset.
		Asset a =  new Asset();
		
		//Set the properties of the asset from the form values in the request.
		try{
			a.setDescription(description);
			a.setAssignedLocationWithId(offices);
			a.setDateImplemented(new Date());
			a.setManufacturer(manufacturer);
			a.setManufacturerPartNum(part);
			a.setNotes(notes);
			a.setOrganizationalTag(tag);
			a.setUser((User)session.getAttribute("user"));
			a.save();
			out.println("Saved!");
		}
		catch (Exception e){
			out.println("Error saving asset.");
			out.println(e.getMessage());
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			out.println(errors.toString());
		}
	}
%>

<div id="content">
<form method="post" action="">
	<label>Current Office/Location:</label>
	<select id = 'offices' name='offices'>
	<option value=""></option>
	<%
		List<Office> availableOffices = Office.getAllOffices();
		for(Office office : availableOffices){
			out.print("<option value='" + office.getId() + "'>" + office.getCityName() + "</option>");
		}
	 %>
	</select>
	<br/>
	
  <label>Organizational Tag:</label>
  <input type="text" id="tag" name="tag" placeholder="Sales" required>

  <label>Manufacturer:</label>
  <input type="text" id="manufacturer" name="manufacturer" placeholder="Acme, Inc." required>

  <label>Manufacturer Part#:</label>
  <input type="text" id="part" name="part" placeholder="123-asdf" required>
  
  <label>Description:</label>
  <input type="text" id="description" name="description" min="1" max="30" required>

   <label>Maintenance notes:</label>
  <input id="notes" name="notes" required>
  	
  <br><br>
  <input type="submit" value="Save Asset" /> 
  
</form>
</div>
<br><br>
<a href="Assets.jsp">View saved assets</a>
</body>
</html>