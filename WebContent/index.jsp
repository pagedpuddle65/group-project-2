<%@page import="edu.a.project2.bo.Asset"%>
<%@page import="edu.a.project2.bo.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../../styles/form.css" type="text/css">
<title>Login Page</title>
</head>
<body>
	<!-- Get the base url that will be used for redirecting later. -->
	<%
		String url = request.getRequestURL().toString();
		String baseURL = url.substring(0, url.length() - request.getRequestURI().length())
				+ request.getContextPath() + "/";
		String indexURL = baseURL + "index.jsp";
	%>

	<!-- If the there is a username set in the request, attemp to login  -->
	<%
		String username = request.getParameter("username");
		if (username != null)
		{
			if (User.userExists(username))
			{
				/* Attempt to login with username and password.  */
				User u = User.getUser(request.getParameter("username"),
						request.getParameter("password"));
				
				/* If the username is not null it means they successfully logged in! Redirect to form.jsp */
				if (u != null)
				{
					session.setAttribute("user", u);
					response.sendRedirect(baseURL + "Form.jsp");
					//session.setAttribute("user", User.getUser(request.getParameter("username"), request.getParameter("password")));
				}
				else
				{
					System.out.println("u is_" + u);
					request.setAttribute("tryAgain", "");
					response.sendRedirect(baseURL + "index.jsp");
				}
			}
			else
			{
				//user doesn't exist
				request.setAttribute("tryAgain", "");
			}

		}
		else//first time
		{
			System.out.println("request.getParameter(username) is " + request.getParameter("username"));
		}
	%>

	<%
		if (request.getAttribute("tryAgain") == null)
		{
			out.println("Please Login before continuing.");
		}
		else
		{
			out.println("Username and password not found. Please try again.");
		}
	%>
	<br>
	<br>
	<form method="post" action=index.jsp>
		<label>Username:</label> <input type="text" id="username"
			name="username" required> <label>Password:</label> <input
			type="password" id="password" name="password" required> <input
			type="submit" value="Login" />
	</form>

</body>
</html>


