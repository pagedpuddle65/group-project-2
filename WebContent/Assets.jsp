<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ page import="edu.a.project2.bo.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../../styles/form.css" type="text/css">
<title>Asset Tracking</title>


</head>
<body>
<!-- setup baseUrl variable that may be used later.! -->
<%
	String url = request.getRequestURL().toString();
	String baseURL = url.substring(0, url.length() - request.getRequestURI().length()) + request.getContextPath() + "/";
%>

<!--if "user" attribute is null in session then go back to index.jsp for user to login -->
<% 
	if((Object)session.getAttribute("user") == null)
	response.sendRedirect(baseURL + "index.jsp"); 
%>

<div id="content">
	<label>Saved Assets:</label>
	<br><br>
	<table class='CSSTableGenerator'>
	<%
		//Get all assets for displaying on the page.
		List<Asset> allAssets = Asset.getAllAssets();
	
		//Print the header of the table.
		out.print("<tr>");
		out.print(printData("Office"));
		out.print(printData("Description"));
		out.print(printData("Manufacturer"));
		out.print(printData("Manufacturer Part Number"));
		out.print(printData("Organizational Tag"));
		out.print(printData("Note"));
		out.print(printData("Date"));
		out.print(printData("User"));
		out.print("</tr>");
		
		//Print the data values out for each asset in the list.
		for(Asset asset : allAssets){
			out.print("<tr>");
			
			out.print(printData(asset.getAssignedLocation().getCityName()));
			out.print(printData(asset.getDescription()));
			out.print(printData(asset.getManufacturer()));
			out.print(printData(asset.getManufacturerPartNum()));
			out.print(printData(asset.getOrganizationalTag()));
			out.print(printData(asset.getNotes()));
			out.print(printData(asset.getDateImplemented().toGMTString()));
			out.print(printData(asset.getUser().getUsername()));
			
			out.print("</tr>");
		}
		
		
	 %>
	 </table>
	<br/>
</div>
</body>
</html>


<%!
//Simple method to help putting the data values into a table cell.
public String printData(String data){
	if(data == null) { data = ""; }
	String result = "<td>" + data + "</td>";
	return result;
}

%>