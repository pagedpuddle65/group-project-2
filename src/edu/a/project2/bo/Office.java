package edu.a.project2.bo;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.DatabaseTable;

/** Office business object where Assets are located. */
@DatabaseTable(tableName = "Office")
public class Office {
	
	@DatabaseField(generatedId = true)
	private UUID id;
	@DatabaseField
	private String CityName;
	
	//Dirty variable for deciding if needs saving.
	private boolean dirty;
	
	/** Helper method to save Office object to office table of database. */
	public void save(){
		ConnectionSource c=null;
		try{
			c = DAOHelper.getConnectionSource();
			Dao<Office, UUID> dao = DaoManager.createDao(c, Office.class);
			
			//If dirty, save in db with update or create.
			if(this.isDirty()){
				dao.createOrUpdate(this);
			}
			c.close();
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		
	}
	
	/** Useful method for finding an office object with a given cityname. */
	public static Office findOfficeForCity(String city){
		Office result = null;
		ConnectionSource c = null;
		
		try{
				c = DAOHelper.getConnectionSource();
				Dao<Office, UUID> dao = DaoManager.createDao(c, Office.class);
				List<Office> list = dao.queryForEq("CityName", city);
				if(list.size() > 0){
					//As of now there should be only 1 city object for a cityname, return it if found.
					result = list.get(0);
				}
				c.close();
		}
		catch (SQLException e){
				e.printStackTrace();
		}
		
		return result;
	}
	
	/** Useful method for returning an office object with a given UUID (as a string). */
	public static Office findOfficeWithId(String Id){
		Office result = null;
		ConnectionSource c = null;
		
		try{
			c = DAOHelper.getConnectionSource();
			Dao<Office, UUID> dao = DaoManager.createDao(c, Office.class);
			//Create UUID object from string and search db for the office.
			result = dao.queryForId(UUID.fromString(Id));
			c.close();
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		
		return result;
	}
	
	/** Return all offices from the db */
	public static List<Office> getAllOffices(){
		List<Office> result = null;
		ConnectionSource c = null;
		
		try{
			c = DAOHelper.getConnectionSource();
			Dao<Office, UUID> dao = DaoManager.createDao(c, Office.class);
			//Query for all offices from the DB.
			result = dao.queryForAll();

			c.close();
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(UUID id) {
		this.id = id;
		this.setDirty();
	}


	/**
	 * @return the cityName
	 */
	public String getCityName()
	{
		return CityName;
	}

	/**
	 * @param cityName the cityName to set
	 */
	public void setCityName(String cityName)
	{
		CityName = cityName;
		this.setDirty();
	}


	/**
	 * @return the dirty
	 */
	public boolean isDirty()
	{
			return dirty;
	}


	/**
	 * @param dirty the dirty to set
	 */
	public void setDirty()
	{
			this.dirty = true;
	}
}
