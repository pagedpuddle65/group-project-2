package edu.a.project2.bo;

import java.sql.SQLException;
import java.util.Date;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/** DAOHelper facilitates communication between the database and the business layer. */
public class DAOHelper {
	
	//phpMyAdmin url for db: https://p3nlmysqladm001.secureserver.net/dgrid50/41/index.php -- Use group2project2 and password Project2@ to login
	private static String databaseUrl = "jdbc:mysql://173.201.88.132:3306/group2project2?user=group2project2&password=Project2@";
	
	/** Return a connectionsource to the database for persisting and reading. */
	public static ConnectionSource getConnectionSource() throws SQLException{
		//As of now we simply create a new connectionsource. In the future we could implement a connectionpool.
        ConnectionSource connectionSource = null;
		connectionSource = new JdbcConnectionSource(databaseUrl);
		return connectionSource;
	}
	
	/** This method completely resets the database and loads some dummy data. Used only for testing. */
	public static String runDAOSetup() throws SQLException{
		String result = "";

		ConnectionSource connectionSource = getConnectionSource();
		
		//Drop the tables
		try{
			TableUtils.dropTable(connectionSource, Asset.class, true);
			TableUtils.dropTable(connectionSource, User.class, true);
			TableUtils.dropTable(connectionSource, Office.class, true);
		}
		catch(Exception e){
			//Do nothing because this is probably fine.
		}
		
		//Create the tables
		TableUtils.createTable(connectionSource, Office.class);
		TableUtils.createTable(connectionSource, User.class);
		TableUtils.createTable(connectionSource, Asset.class);
		
		//Create dummy Offices
		Office office = new Office();
		office.setCityName("San Diego");
		office.save();
		
		Office office1 = new Office();
		office1.setCityName("Salt Lake City");
		office1.save();
		
		Office office2 = new Office();
		office2.setCityName("Austin");
		office2.save();
		
		Office office3 = new Office();
		office3.setCityName("Seattle");
		office3.save();

		//Create some Users
		User u = new User();
		u.setUsername("shieldst");
		u.setPassword("troy");
		u.save();
		
		User u2 = new User();
		u2.setUsername("daniel");
		u2.setPassword("daniel");
		u2.save();
		
		User u3 = new User();
		u3.setUsername("colton");
		u3.setPassword("colton");
		u3.save();
		
		User u4 = new User();
		u4.setUsername("doug");
		u4.setPassword("doug");
		u4.save();
		
		//Create an Asset
		Asset asset1 = new Asset();
		asset1.setDateImplemented(new Date());
		asset1.setDescription("This is a great asset for testing.");
		asset1.setManufacturer("Foxconn");
		asset1.setManufacturerPartNum("12345");
		asset1.setOrganizationalTag("Quality Assurance");	
		asset1.setAssignedLocation(office);
		asset1.setUser(u);
		asset1.save();
		
		connectionSource.close();
		
		//Return a message to display.
		result = "Reset successful.";
		return result;
	
	}

}
