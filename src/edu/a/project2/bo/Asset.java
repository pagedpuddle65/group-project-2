package edu.a.project2.bo;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.DatabaseTable;

/** Asset object that represents IT assets in the organization. */
@DatabaseTable(tableName = "Asset")
public class Asset {
	
	@DatabaseField(generatedId = true)
	private UUID			id;
	@DatabaseField(foreign = true)
	private Office			assignedLocation;
	@DatabaseField(foreign = true)
	private User			user;
	@DatabaseField
	private String			organizationalTag;
	@DatabaseField
	private String			manufacturer;
	@DatabaseField
	private String			manufacturerPartNum;
	@DatabaseField
	private String			description;
	@DatabaseField
	private Date			dateImplemented;
	@DatabaseField
	private String			notes;
	
	//dirty variable to decide whether or not this object needs to be saved to the database.
	private boolean         dirty;


	/** Save is a helper method that saves this object to the database if necessary. */
	public void save() throws SQLException{
		ConnectionSource c = null;
		//Get a connectionSource from the DAOHelper.
		c = DAOHelper.getConnectionSource();
		//Create an assetDao for saving to the Asset table in the Database.
		Dao<Asset, UUID> assetDao = DaoManager.createDao(c, Asset.class);
		
		//If this object is dirty, save it to the database with either a create or update command.
		if(this.isDirty()){
			assetDao.createOrUpdate(this);
		}
		c.close();
	}
	
	/** Return all assets from the Db. */
	public static List<Asset> getAllAssets(){
		List<Asset> result = null;
		ConnectionSource c = null;
		
		try{
			c = DAOHelper.getConnectionSource();
			Dao<Asset, UUID> dao = DaoManager.createDao(c, Asset.class);
			
			//Get all asset tuples from the database in a list of Assets.
			result = dao.queryForAll();

			c.close();
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	/**
	 * @return the id
	 */
	public UUID getId()
	{
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(UUID id)
	{
		this.id = id;
		this.setDirty();
	}

	/**
	 * @return the assignedLocation
	 */
	public Office getAssignedLocation()
	{
		if(assignedLocation.getCityName() == null){
			assignedLocation = Office.findOfficeWithId(assignedLocation.getId().toString());
		}
		return assignedLocation;
	}

	/**
	 * @param assignedLocation
	 *            the assignedLocation to set
	 */
	public void setAssignedLocation(Office assignedLocation)
	{
		this.assignedLocation = assignedLocation;
		this.setDirty();
	}

	/**
	 * @param assignedLocation
	 *            the assignedLocation to set
	 */
	public void setAssignedLocationWithId(String assignedLocationId)
	{
		this.setAssignedLocation(Office.findOfficeWithId(assignedLocationId));
	}

	/**
	 * @return the organizationalTag
	 */
	public String getOrganizationalTag()
	{
		return organizationalTag;
	}

	/**
	 * @param organizationalTag
	 *            the organizationalTag to set
	 */
	public void setOrganizationalTag(String organizationalTag)
	{
		this.organizationalTag = organizationalTag;
		this.setDirty();
	}

	/**
	 * @return the manufacturer
	 */
	public String getManufacturer()
	{
		return manufacturer;
	}

	/**
	 * @param manufacturer
	 *            the manufacturer to set
	 */
	public void setManufacturer(String manufacturer)
	{
		this.manufacturer = manufacturer;
		this.setDirty();
	}

	/**
	 * @return the manufacturerPartNum
	 */
	public String getManufacturerPartNum()
	{
		return manufacturerPartNum;
	}

	/**
	 * @param manufacturerPartNum
	 *            the manufacturerPartNum to set
	 */
	public void setManufacturerPartNum(String manufacturerPartNum)
	{
		this.manufacturerPartNum = manufacturerPartNum;
		this.setDirty();
	}

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description)
	{
		this.description = description;
		this.setDirty();
	}

	/**
	 * @return the dateImplemented
	 */
	public Date getDateImplemented()
	{
		return dateImplemented;
	}

	/**
	 * @param dateImplemented
	 *            the dateImplemented to set
	 */
	public void setDateImplemented(Date dateImplemented)
	{
		this.dateImplemented = dateImplemented;
		this.setDirty();
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
		this.setDirty();
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		if(user.getUsername() == null){
			user = User.getUserWithId(user.getId());
		}
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the dirty
	 */
	public boolean isDirty()
	{
		return dirty;
	}

	/**
	 * @param dirty the dirty to set
	 */
	public void setDirty()
	{
		this.dirty = true;
	}
}
