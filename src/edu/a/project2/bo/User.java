package edu.a.project2.bo;

import java.sql.SQLException;
import java.util.UUID;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.DatabaseTable;

/** User object for logging in and submitting new assets. */
@DatabaseTable(tableName = "Users")
public class User {

	private static final String	USERNAME_FIELD_NAME	= "username";

	@DatabaseField(generatedId = true)
	private UUID				id;
	@DatabaseField(canBeNull = false, columnName = USERNAME_FIELD_NAME)
	private String				username;
	@DatabaseField
	private String				password;
	
	//Dirty value for deciding if needs to be saved.
	private boolean				dirty;
	
	//Dao and connectionsource for interacting with the user table.
	private Dao<User, UUID>		userDao;
	private ConnectionSource	connectionSource;

	/** Method for changing the users password. */
	public PasswordChangeMessage changePassword(String oldPassword, String newPassword, String confirmPassword)
	{
		//Make sure they have authenticated their right to change the password.
		if (oldPassword.equals(password))
		{
			if (newPassword.equals(confirmPassword))
			{
				// change password in DB
				try
				{
					connectDao();
					User u = userDao.queryForId(id);
					u.setPassword(newPassword);
					u.save();
					closeDao();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}

				// confirm to USER
				return PasswordChangeMessage.SUCCESS;
			}
			else
			{
				return PasswordChangeMessage.WRONGNEW;
			}
		}
		else
		{
			return PasswordChangeMessage.WRONGPASSWORD;
		}
	}
	
	/** Check if this user exists in the database. */
	public static boolean userExists(String username){
		User u;
		try
		{
			ConnectionSource connectionSource = DAOHelper.getConnectionSource();
			Dao<User, UUID> tempDao = DaoManager.createDao(connectionSource, User.class);
			QueryBuilder<User, UUID> qb = tempDao.queryBuilder();
			//Query user table for a user with this username.
			qb.where().eq(User.USERNAME_FIELD_NAME, username);
			PreparedQuery<User> pq = qb.prepare();

			u = tempDao.queryForFirst(pq);
			connectionSource.close();
			
			//If we have found a user then return true, otherwise false.
			if (u!=null)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return false;
	}
	
	/** Check for a user with a username and password. Used for logging in. */
	public static User getUser(String username, String passwordEntered){
		User u = null;
		
		try
		{
			ConnectionSource connectionSource = DAOHelper.getConnectionSource();
			Dao<User, UUID> tempDao = DaoManager.createDao(connectionSource, User.class);
			QueryBuilder<User, UUID> qb = tempDao.queryBuilder();
			//Get the user for this username.
			qb.where().eq(User.USERNAME_FIELD_NAME, username);
			PreparedQuery<User> pq = qb.prepare();

			u = tempDao.queryForFirst(pq);
			connectionSource.close();
			
			//Check that this user's password matches the password submitted. 
			if (u.getPassword().equals(passwordEntered))
			{
				//If true, user successfully logs in.
				return u;
			}
			else
			{
				//If false, return null.
				return null;
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		return null;
	}

	/** Method to create the connectionSource and userDao objects for communicating with User table in db. */
	public void connectDao()
	{
		try
		{
			connectionSource = DAOHelper.getConnectionSource();
			userDao = DaoManager.createDao(connectionSource, User.class);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	/** Close DAO when finished with it. */
	public void closeDao()
	{
		try
			{
				connectionSource.close();
			}
		catch (SQLException e)
			{
				e.printStackTrace();
			}
	}

	/** Helper method to save this object to the database. */
	public void save()
	{
		ConnectionSource c = null;
		try
		{
			c = DAOHelper.getConnectionSource();
			Dao<User, UUID> userDao = DaoManager.createDao(c, User.class);
			if (this.isDirty())
			{
				//Save this user if dirty with either create or update command.
				userDao.createOrUpdate(this);
			}
			c.close();
		}
		catch (SQLException e)
		{// so that we don't have to use Try/Catches all over the place.
			e.printStackTrace();
		}

	}
	
	/** Get a user object with the user's UUID */
	public static User getUserWithId(UUID id2) {
		User result = null;
		ConnectionSource c = null;
		
		try{
			c = DAOHelper.getConnectionSource();
			Dao<User, UUID> dao = DaoManager.createDao(c, User.class);
			//Query for user with given UUID
			result = dao.queryForId(id2);
			c.close();
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		
		return result;
	}


	/**
	 * @return the id
	 */
	public UUID getId()
	{
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(UUID id)
	{
		this.id = id;
		setDirty();
	}

	/**
	 * @return the username
	 */
	public String getUsername()
	{
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username)
	{
		this.username = username;
		setDirty();
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
		setDirty();
	}

	/**
	 * @return the dirty
	 */
	public boolean isDirty()
	{
		return dirty;
	}

	/**
	 * @param dirty
	 *            the dirty to set
	 */
	public void setDirty()
	{
		this.dirty = true;
	}
}
